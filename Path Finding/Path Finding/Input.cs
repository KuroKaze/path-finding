﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

class Input
{
    private KeyboardState oldKeyState;
    private MouseState oldMouseState;
    private Texture2D cursorSpriteMap;
    private const int unClickFrame = 1, clickFrame = 2, loadFrame = 3;
    private int curCursorFrame;

    /// <summary>
    /// Constuctor
    /// </summary>
    /// <param name="game">Game object to make mouse visible or not</param>
    /// <param name="texture">Cursor sprite map</param>
    public Input(Game game, Texture2D texture = null)
    {
        cursorSpriteMap = texture;
        if (texture == null)
            game.IsMouseVisible = true;
        else
            game.IsMouseVisible = false;
        curCursorFrame = 1;
    }

    /// <summary>
    /// Set up window
    /// </summary>
    /// <param name="graphics">Access to window settings</param>
    /// <param name="fullScreen">Set fullscreen (default: false)</param>
    /// <param name="winWidth">Set window width (default: 1280px)</param>
    /// <param name="windHeight">Set window height (default: 720px)</param>
    public void initGraphics(GraphicsDeviceManager graphics,  bool fullScreen = false, int winWidth = 1280, int windHeight = 720)
    {
        if (!fullScreen)
        {
            if (winWidth <= GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width && windHeight <= GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height)
            {
                graphics.PreferredBackBufferWidth = winWidth;
                graphics.PreferredBackBufferHeight = windHeight;
                graphics.IsFullScreen = fullScreen;
                graphics.ApplyChanges();
            }
        }
        else
        {
            foreach (DisplayMode dm in GraphicsAdapter.DefaultAdapter.SupportedDisplayModes)
            {
                if (dm.Width == winWidth && dm.Height == windHeight)
                {
                    graphics.PreferredBackBufferWidth = winWidth;
                    graphics.PreferredBackBufferHeight = windHeight;
                    graphics.IsFullScreen = fullScreen;
                    graphics.ApplyChanges();
                }
            }
        }
    }

    /// <summary>
    /// Left mouse button was clicked or not
    /// </summary>
    /// <returns>True if left mouse button was clicked false otherwise</returns>
    public bool lMouseButtonClicked()
    {
        MouseState newMouseState = Mouse.GetState();
        return newMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released;
    }

    /// <summary>
    /// Left mouse button was clicked on entity or not
    /// </summary>
    /// <param name="ent">Entity being clicked on</param>
    /// <returns>True if left mouse button was clicked on entity false otherwise</returns>
    public bool lMouseBottonClickedOn(Entity ent)
    {
        MouseState newMouseState = Mouse.GetState();
        bool sameLocation = newMouseState.X == oldMouseState.X && newMouseState.Y == oldMouseState.Y;
        bool clicked = newMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released;
        if (clicked && sameLocation)
        {
            return newMouseState.X >= ent.boundingBox.X &&
                   newMouseState.X <= ent.boundingBox.X + ent.boundingBox.Width &&
                   newMouseState.Y >= ent.boundingBox.Y &&
                   newMouseState.Y <= ent.boundingBox.Y + ent.boundingBox.Height;
        }
        else
            return false;
    }

    /// <summary>
    /// Key been pressed
    /// </summary>
    /// <param name="key">Key that is being checked for</param>
    /// <returns>True if key was press (not held down), false otherwise</returns>
    public bool keyPressed(Keys key)
    {
        KeyboardState newKeyState = Keyboard.GetState();
        return newKeyState.IsKeyUp(key) && oldKeyState.IsKeyDown(key);
    }

    /// <summary>
    /// Key being held down
    /// </summary>
    /// <param name="key">Key that is being checked for</param>
    /// <returns>True if key is being held down, false otherwise</returns>
    public bool keyHeld(Keys key)
    {
        return Keyboard.GetState().IsKeyDown(key);
    }

    /// <summary>
    /// Updates the keyboard and mouse states and change cursor sprite when clicked
    /// </summary>
    public void Update()
    {
        MouseState newMouseState = Mouse.GetState();
        KeyboardState newKeyState = Keyboard.GetState();
        if (cursorSpriteMap != null && newMouseState.LeftButton == ButtonState.Pressed)
            curCursorFrame = 2;
        else if (cursorSpriteMap != null && newMouseState.LeftButton == ButtonState.Released)
            curCursorFrame = 1;
        oldMouseState = newMouseState;
        oldKeyState = newKeyState;
    }

    /// <summary>
    /// Draw the cursor
    /// </summary>
    /// <param name="spriteBatch">Enables cursor to be drawn</param>
    public void Draw(SpriteBatch spriteBatch)
    {
        if (cursorSpriteMap != null)
        {
            int row = curCursorFrame / 3;
            int column = curCursorFrame % 3;
            int width = cursorSpriteMap.Width / 3;
            int height = cursorSpriteMap.Height / 3;
            Rectangle srcRectangle = new Rectangle((width) * column, (height) * row, width, height);
            spriteBatch.Draw(cursorSpriteMap, new Vector2((float)Mouse.GetState().X, (float)Mouse.GetState().Y), srcRectangle, Color.White);
        }
    }
}