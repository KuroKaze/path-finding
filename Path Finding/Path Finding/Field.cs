﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Path_Finding
{
    class Field
    {
        public List<Entity> grid;

        public Field(ContentManager content)
        {
            grid = new List<Entity>();
            for (int i = 0; i < Path_Finding.Game1.WINHEIGHT / 25; i++)
            {
                for (int j = 0; j < Path_Finding.Game1.WINWIDTH / 25; j++)
                {
                    grid.Add(new Point(content.Load<Texture2D>("Assets\\Point"), 1, 7, 0, 2, new Vector2(j * 25.0f + 25.0f / 2.0f, i * 25.0f + 25.0f / 2.0f)));
                }
            }

            Console.WriteLine("White: Normal\n" + 
            "Black: Blocked(Click)\n" +
            "Red: Checked\n" +
            "Green: Path\n" +
            "Yellow: In frontier\n" +
            "Blue: Start(left Ctrl+Click)\n" +
            "Light Blue: Goal(left Alt+Click)\n" +
            "Enter to start path finding(make sure there is only 1 start and 1 end point!!!)\n" +
            "'r' to reset all nodes\n" +
            "left Ctrl+'p' reset path\n" +
            "Heuristics 0 = Diagonal(L_inf norm); 1 = Manhattan(L1 norm, Default); 2 = Euclidean(L2 norm); 3 = Dijkstra");
        }

        public void resetPath()
        {
            foreach (Entity ent in grid)
            {
                if (ent.currentFrame > 1)
                    ent.currentFrame = 0;
            }
        }

        public void Update(Input mouseKey)
        {
            if(mouseKey.lMouseButtonClicked())
            {
                foreach (Entity ent in grid)
                {
                    if (mouseKey.lMouseBottonClickedOn(ent) && mouseKey.keyHeld(Keys.LeftControl))
                        ((Point)ent).doWhenClicked(5);
                    else if (mouseKey.lMouseBottonClickedOn(ent) && mouseKey.keyHeld(Keys.LeftAlt))
                        ((Point)ent).doWhenClicked(6);
                    else if (mouseKey.lMouseBottonClickedOn(ent))
                        ent.doWhenClicked();
                }
            }
            mouseKey.Update();
        }

        public List<Entity> findNeighbor(Entity ent)
        {
            List<Entity> neighbors = new List<Entity>();
            int idx = grid.FindIndex(
                delegate(Entity entity)
                {
                    return entity.Location == ent.Location;
                });
            int row = idx / (Path_Finding.Game1.WINWIDTH / 25);
            int col = idx % (Path_Finding.Game1.WINWIDTH / 25);
            bool right = col >= 0 && col < (Path_Finding.Game1.WINWIDTH / 25) - 1;
            bool left = col <= (Path_Finding.Game1.WINWIDTH / 25) - 1 && col > 0;
            bool bottom = row >= 0 && row < (Path_Finding.Game1.WINHEIGHT / 25) - 1;
            bool top = row <= (Path_Finding.Game1.WINHEIGHT / 25) - 1 && row > 0;
            if (right && grid[index(row, col + 1)].currentFrame != 1)
                neighbors.Add(grid[index(row, col + 1)]);
            if (left && grid[index(row, col - 1)].currentFrame != 1)
                neighbors.Add(grid[index(row, col - 1)]);
            if (bottom && grid[index(row + 1, col)].currentFrame != 1)
                neighbors.Add(grid[index(row + 1, col)]);
            if (top && grid[index(row - 1, col)].currentFrame != 1)
                neighbors.Add(grid[index(row - 1, col)]);
            if (right && bottom && grid[index(row + 1, col + 1)].currentFrame != 1)
                neighbors.Add(grid[index(row + 1, col + 1)]);
            if (left && bottom && grid[index(row + 1, col - 1)].currentFrame != 1)
                neighbors.Add(grid[index(row + 1, col - 1)]);
            if (right && top && grid[index(row - 1, col + 1)].currentFrame != 1)
                neighbors.Add(grid[index(row - 1, col + 1)]);
            if (left && top && grid[index(row - 1, col - 1)].currentFrame != 1)
                neighbors.Add(grid[index(row - 1, col - 1)]);
            return neighbors;
        }

        public int index(int row, int column)
        {
            return row * (Path_Finding.Game1.WINWIDTH / 25) + column;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Entity ent in grid)
                ent.Draw(spriteBatch);
        }
    }
}
