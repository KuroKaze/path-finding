using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Path_Finding
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        public const int WINHEIGHT = 25 * 20;
        public const int WINWIDTH = 25 * 20;

        Input mouseKey;
        Field field;
        FindPath find;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            mouseKey = new Input(this);
            mouseKey.initGraphics(graphics, false, WINWIDTH, WINHEIGHT);
            find = new FindPath();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            field = new Field(Content);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (mouseKey.keyPressed(Keys.Escape))
                this.Exit();
            if (mouseKey.keyPressed(Keys.Enter))
                find.runPath(field);
            if (mouseKey.keyPressed(Keys.R))
            {
                field = new Field(Content);
                find = new FindPath();
            }
            if (mouseKey.keyHeld(Keys.LeftControl) && mouseKey.keyPressed(Keys.P))
                field.resetPath();
            if (mouseKey.keyPressed(Keys.D0))
                find.HChoice = 0;
            if (mouseKey.keyPressed(Keys.D1))
                find.HChoice = 1;
            if (mouseKey.keyPressed(Keys.D2))
                find.HChoice = 2;
            if (mouseKey.keyPressed(Keys.D3))
                find.HChoice = 3;

            field.Update(mouseKey);

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            field.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
