﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Path_Finding
{
    class FindPath
    {
        Entity begin, current, goal;
        LinkedList<Tuple<float, float, Entity>> openSet, closedSet;
        List<Tuple<Entity, Entity>> path;
        bool solvable;
        int hChoice = 1;
        float distance = 0.0f;

        public int HChoice 
        {
            get
            {
                return hChoice;
            }
            set
            {
                hChoice = value;
                Console.WriteLine("heuristic: " + hChoice);
            }
        }

        public FindPath()
        {
            openSet = new LinkedList<Tuple<float, float, Entity>>();
            closedSet = new LinkedList<Tuple<float, float, Entity>>();
            path = new List<Tuple<Entity,Entity>>();
            solvable = false;
        }

        public void runPath(Field field)
        {
            openSet = new LinkedList<Tuple<float, float, Entity>>();
            closedSet = new LinkedList<Tuple<float, float, Entity>>();
            path = new List<Tuple<Entity, Entity>>();
            distance = 0.0f;
            solvable = false;

            begin = field.grid.Find(
                delegate(Entity ent)
                {
                    return ent.currentFrame == 5;
                });
            goal = field.grid.Find(
                delegate(Entity ent)
                {
                    return ent.currentFrame == 6;
                });

            addToOpen(heuristic(begin, goal), 0.0f, begin);

            while (openSet.Count > 0)
            {
                current = openSet.First().Item3;
                ((Point)current).doWhenClicked(2);
                //Console.WriteLine(current.Location.ToString());
                addToClose(openSet.First().Item1, openSet.First().Item2, current);
                float fScore = openSet.First().Item1;
                float gScore = openSet.First().Item2;
                openSet.RemoveFirst();
                if (current.Location == goal.Location)
                {
                    constructPath(current);
                    solvable = true;
                    Console.WriteLine("Distance traveled: " + distance);
                    break;
                }
                foreach (Entity neighbor in field.findNeighbor(current))
                {
                    if (!closedContains(neighbor))
                    {
                        float ngScore = gScore + cost(current, neighbor);
                        float priority = heuristic(neighbor, goal) + ngScore;
                        if (!openContains(neighbor))
                        {
                            addToOpen(priority, ngScore, neighbor);
                            path.Add(new Tuple<Entity, Entity>(current, neighbor));
                            ((Point)neighbor).doWhenClicked(4);
                        }
                        else
                        {
                            int idx = openContainsIdx(neighbor);
                            if (openSet.ElementAt(idx).Item2 > ngScore)
                            {
                                openSet.Remove(openSet.Find(openSet.ElementAt(idx)));
                                addToOpen(priority, ngScore, neighbor);
                                path.Add(new Tuple<Entity, Entity>(current, neighbor));
                            }
                        }
                    }
                }
            }

            if(!solvable)
                Console.WriteLine("Trapped!!!!!!!!!!!!!!!!!!");
        }

        private void constructPath(Entity entity)
        {
            if (entity != begin)
            {
                for (int i = path.Count - 1; i >= 0; i--)
                {
                    if (path[i].Item2 == entity)
                    {
                        constructPath(path[i].Item1);
                        ((Point)entity).doWhenClicked(3);
                        distance += cost(entity, path[i].Item1);
                        break;
                    }
                }
            }
            else
                ((Point)entity).doWhenClicked(3);
        }

        private void addToOpen(float priority, float gScore, Entity entity)
        {
            if (openSet.Count > 0)
            {

                if (openSet.Last().Item1 <= priority)
                    openSet.AddLast(new Tuple<float, float, Entity>(priority, gScore, entity));
                else
                {
                    for (int i = 0; i < openSet.Count; i++)
                    {
                        if (openSet.ElementAt(i).Item1 > priority)
                        {
                            openSet.AddBefore(openSet.Find(openSet.ElementAt(i)), new Tuple<float, float, Entity>(priority, gScore, entity));
                            break;
                        }
                    }
                }
            }
            else
                openSet.AddFirst(new Tuple<float, float, Entity>(priority, gScore, entity));
        }

        private bool openContains(Entity entity)
        {
            for (int i = 0; i < openSet.Count; i++)
            {
                if (openSet.ElementAt(i).Item3 == entity )
                    return true;
            }
            return false;
        }

        private int openContainsIdx(Entity entity)
        {
            for (int i = 0; i < openSet.Count; i++)
            {
                if (openSet.ElementAt(i).Item3 == entity)
                    return i;
            }
            return 0;
        }

        private void addToClose(float priority, float gScore, Entity entity)
        {
            if (closedSet.Count > 0)
            {
                if (closedSet.Last().Item1 <= priority)
                    closedSet.AddLast(new Tuple<float, float, Entity>(priority, gScore, entity));
                else
                {
                    for (int i = 0; i < closedSet.Count; i++)
                    {
                        if (closedSet.ElementAt(i).Item1 > priority)
                        {
                            closedSet.AddBefore(closedSet.Find(closedSet.ElementAt(i)), new Tuple<float, float, Entity>(priority, gScore, entity));
                            break;
                        }
                    }
                }
            }
            else
                closedSet.AddFirst(new Tuple<float, float, Entity>(priority, gScore, entity));
        }

        private bool closedContains(Entity entity)
        {
            for (int i = 0; i < closedSet.Count; i++)
            {
                if (closedSet.ElementAt(i).Item3 == entity )
                    return true;
            }
            return false;
        }

        //g(x)
        private float cost(Entity start, Entity end)
        {
            return (float)Math.Sqrt(Math.Pow(start.Location.X - end.Location.X, 2) + Math.Pow(start.Location.Y - end.Location.Y, 2));
        }

        //h(x)
        private float heuristic(Entity start, Entity end)
        {
            //Diagonal L_inf norm
            if (hChoice == 0)
                return Math.Max(Math.Abs(start.Location.X - end.Location.X), Math.Abs(start.Location.Y - end.Location.Y));
            //Manhattan L1 norm
            else if (hChoice == 1)
                return Math.Abs(start.Location.X - end.Location.X) + Math.Abs(start.Location.Y - end.Location.Y);
            //Euclidean L2 norm
            else if (hChoice == 2)
                return (float)Math.Sqrt(Math.Pow(start.Location.X - end.Location.X, 2) + Math.Pow(start.Location.Y - end.Location.Y, 2));
            //Dijkstra
            else
                return 0;
        }
    }
}
