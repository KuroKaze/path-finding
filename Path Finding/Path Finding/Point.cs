﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Path_Finding
{
    class Point : Entity
    {
        public Point(Texture2D texture, int numRows, int numColumns, int startFrame, int endFrame, Vector2 loc, bool ftl = false)
            : base(texture, numRows, numColumns, startFrame, endFrame, loc, ftl)
        {

        }

        public override void doWhenClicked()
        {
            base.Update();
        }

        public void doWhenClicked(int frame)
        {
            currentFrame = frame;
        }
    }
}
