﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


class Collision
{
    private List<Entity> allEntities;
    private Rectangle screenBoundary;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="screenL">Boundary for left side of screen</param>
    /// <param name="screenT">Boundary for top of screen</param>
    /// <param name="screenR">Boundary for right side of screen</param>
    /// <param name="screenB">Boundary for bottom of screen</param>
    public Collision(int screenL, int screenT, int screenR, int screenB)
    {
        allEntities = new List<Entity>();
        screenBoundary = new Rectangle(screenL, screenT, screenR, screenB);
    }

    /// <summary>
    /// Add entity to list to do collision detection
    /// </summary>
    /// <param name="ent">Entity tobe added</param>
    public virtual void add(Entity ent)
    {
        allEntities.Add(ent);
    }

    /// <summary>
    /// Add a list of entities
    /// </summary>
    /// <param name="listEnt">List of entities tobe added</param>
    public virtual void add(List<Entity> listEnt)
    {
        allEntities.AddRange(listEnt);
    }

    /// <summary>
    /// Remove entity from list (it can't collide with anything anymore)
    /// </summary>
    /// <param name="ent">Entity tobe removed</param>
    public virtual void remove(Entity ent)
    {
        allEntities.Remove(ent);
    }

    /// <summary>
    /// Remove entity from list
    /// </summary>
    /// <param name="idx">Index of entity tobe removed</param>
    public virtual void remove(int idx)
    {
        allEntities.RemoveAt(idx);
    }

    /// <summary>
    /// Check of entity has moved off screen
    /// </summary>
    /// <param name="obj">Entity you want to check</param>
    /// <returns>True if off screen</returns>
    public virtual bool moveOffScreen(Entity obj)
    {
        return hitLeftScreen(obj) || hitTopScreen(obj) || hitRightScreen(obj) || hitBottomScreen(obj);
    }

    public virtual bool hitRightScreen(Entity obj)
    {
        return obj.boundingBox.X + obj.width >= screenBoundary.Width;
    }

    public virtual bool hitLeftScreen(Entity obj)
    {
        return obj.boundingBox.X <= screenBoundary.X;
    }

    public virtual bool hitTopScreen(Entity obj)
    {
        return obj.boundingBox.Y <= screenBoundary.Y;
    }

    public virtual bool hitBottomScreen(Entity obj)
    {
        return obj.boundingBox.Y + obj.height >= screenBoundary.Height;
    }

    /// <summary>
    /// Check of entity is colliding with another entity
    /// </summary>
    /// <param name="obj">Entity being checked</param>
    /// <returns>Entity that collided with entity that was passed in</returns>
    public virtual Entity collide(Entity obj)
    {
        int idx = allEntities.FindIndex(
            delegate(Entity ent)
            {
                return ent.Location == obj.Location;
            });
        for (int i = 0; i < allEntities.Count; i++)
        {
            if (i != idx)
            {
                if (obj.boundingBox.X < allEntities[i].boundingBox.X + allEntities[i].width &&
                   obj.boundingBox.X + obj.boundingBox.Width > allEntities[i].boundingBox.X &&
                   obj.boundingBox.Y < allEntities[i].boundingBox.Y + allEntities[i].height &&
                   obj.boundingBox.Y + obj.boundingBox.Height > allEntities[i].boundingBox.Y)
                    return allEntities[i];
            }
        }
        return null;
    }

    /// <summary>
    /// Check if space is ocuppied by an enity
    /// </summary>
    /// <param name="rect">Space being checked</param>
    /// <returns>Enitiy that is ocupping that space</returns>
    public virtual Entity collide(Rectangle rect)
    {
        for (int i = 0; i < allEntities.Count; i++)
        {
            if (rect.X < allEntities[i].boundingBox.X + allEntities[i].width &&
                rect.X + rect.Width > allEntities[i].boundingBox.X &&
                rect.Y < allEntities[i].boundingBox.Y + allEntities[i].height &&
                rect.Y + rect.Height > allEntities[i].boundingBox.Y)
                return allEntities[i];
        }
        return null;
    }
}